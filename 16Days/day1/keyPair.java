import java.util.*;
class Solution {
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        // code here
        HashSet<Integer> hs = new HashSet<Integer>();
        for(int i=n-1; i>=0; i--){
            int temp = x-arr[i];
            if(hs.contains(temp)){
                return true;
            }else{
                hs.add(arr[i]);
            }
        }
        return false;
    }
}
class Client{

	public static void main(String[] args){

		System.out.println("Enter array size");
		Scanner sc = new Scanner(System.in);

		int Size = sc.nextInt();
		System.out.println("Enter sum");
		int Sum = sc.nextInt();
		
		int arr[] = new int[Size];

		System.out.println("Enter array element");
		for(int i=0;i<Size; i++){

			arr[i] = sc.nextInt();
		}

		Solution obj = new Solution();
		boolean ret = obj.hasArrayTwoCandidates(arr,Size,Sum);

		System.out.println(ret);
	}
}
