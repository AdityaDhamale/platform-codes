import java.util.Arrays;
import java.util.*;

class Solution {
    public int singleNumber(int[] nums) {
        Arrays.sort(nums);
        for(int i=0; i<nums.length-1; i++){
            if(nums[i]==nums[i+1]){
                i++;
            }else{
                return nums[i];
            }
        }
        return nums[nums.length-1];
    }
}
class Client{

	public static void main(String[] args){

		System.out.println("Enter array size");
		Scanner sc = new Scanner(System.in);

		int Size = sc.nextInt();
		int arr[] = new int[Size];

		System.out.println("Enter array element");
		for(int i=0;i<Size; i++){

			arr[i] = sc.nextInt();
		}

		Solution obj = new Solution();
		int ret = obj.singleNumber(arr);

		System.out.println(ret);
	}
}
