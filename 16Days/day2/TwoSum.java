import java.util.*;
class Solution {
    public int[] twoSum(int[] nums, int target) {

        int result[] = new int[2];
        for(int i=0; i<nums.length; i++){
            for (int j = i + 1; j < nums.length; j++) {
                if(nums[i]+nums[j]==target){
                    result[0] = i;
                    result[1] = j;
                    return result;
                }
            }
        }
        return result;
    }
}
class Client{

	public static void main(String [] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter target sum");
		int sum = sc.nextInt();

		System.out.println("Enter array size");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter array elements");
		for(int i=0; i<n; i++){

			arr[i] = sc.nextInt();
		}

		Solution obj = new Solution();
		int a[] = obj.twoSum(arr,sum);
		System.out.print("[");
		for(int i=0; i<2; i++){

			System.out.print(a[i]+" ");
		}
		System.out.print("]");
		System.out.println();
	}
}
